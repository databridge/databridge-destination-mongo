# DEPRECATED

No longer setup as separate packages. See [databridge on github](https://github.com/psalmody/databridge)



> Installs in [databridge](https://gitlab.com/databridge/databridge) module.

From the DataBridge module directory:

```shell
npm install --save databridge-destination-mongo
```
